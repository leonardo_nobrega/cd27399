(defproject cd27399 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src"]
  :test-paths ["test"]
  :dependencies [[cenx/pack "0.1.5"]
                 [org.clojure/clojure "1.6.0"]
                 [commons-io/commons-io "2.5"]]
  :profiles {:dev {:source-paths ["dev"]
                   :dependencies [[org.clojure/tools.namespace "0.2.10"]]}}
  :repositories ^:replace [["nexus" {:url "http://nexus.cenx.localnet:8081/nexus/content/groups/public"}]])
