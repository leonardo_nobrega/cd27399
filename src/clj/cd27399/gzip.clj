(ns cd27399.gzip
  (:require [cd27399.core :as core]
            [cenx.pack.core])
  (:import [java.io ByteArrayInputStream ByteArrayOutputStream IOException]
           [java.util.zip GZIPInputStream GZIPOutputStream]
           org.apache.commons.io.IOUtils))

(defn dump-byte-arr
  [byte-arr num-cols]
  (let [byte->hex-str (fn [i] (->> i
                                   (nth byte-arr)
                                   (format " %02x")))
        byte->char-str (fn [i] (let [b (nth byte-arr i)]
                                 (if (and (<= 32 b) (< b 127))
                                   (-> b char str)
                                   ".")))
        pad (fn [n] (apply str (repeat n "   ")))]
    (loop [col 0
           i 0
           hex-str ""
           char-str ""
           dump-str ""]
      (if (< i (count byte-arr))
        (cond (= col num-cols)
              (recur 1
                     (inc i)
                     (byte->hex-str i)
                     (byte->char-str i)
                     (str dump-str hex-str " " char-str "\n"))
              :else
              (recur (inc col)
                     (inc i)
                     (str hex-str (byte->hex-str i))
                     (str char-str (byte->char-str i))
                     dump-str))
        (str dump-str
             hex-str " " (pad (- num-cols col)) char-str)))))

(defn print-bytes
  ([byte-arr]
   (print-bytes byte-arr 8))
  ([byte-arr num-cols]
   (dump-byte-arr byte-arr num-cols)))

(defn compress
  [str]
  (let [in-stream (->> str
                       (.getBytes)
                       (new ByteArrayInputStream))
        byte-out-stream (new ByteArrayOutputStream)
        gzip-out-stream (new GZIPOutputStream byte-out-stream)]
    (try (IOUtils/copy in-stream gzip-out-stream)
         (.finish gzip-out-stream)
         (catch IOException e
           (println e)))
    (.toByteArray byte-out-stream)))

(defn decompress
  [byte-arr]
  (let [in-stream (->> byte-arr
                       (new ByteArrayInputStream)
                       (new GZIPInputStream))
        out-stream (new ByteArrayOutputStream)]
    (try (IOUtils/copy in-stream out-stream)
         (catch IOException e
           (println e)))
    (->> out-stream
         .toByteArray
         (new String))))

(defn load+transit-encode
  [path]
  (-> path
      core/load-obj
      core/get-docs
      cenx.pack.core/render-transit))

(defn load+compress+count
  [path]
  (-> path
      load+transit-encode
      compress
      count))

(defn compress-decompress-test
  [path]
  (let [encoded-obj (load+transit-encode path)
        compressed (compress encoded-obj)
        decompressed (decompress compressed)]
    (= encoded-obj decompressed)))
