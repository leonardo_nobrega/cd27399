(ns cd27399.stats
  (:require [cd27399.core :as core]))

(defn doc-keys
  [docs]
  (->> docs
       (map keys)
       (map set)
       (apply clojure.set/intersection)))

(defn value-lengths
  [obj]
  (let [docs (core/get-docs obj)
        ks (doc-keys docs)]
    (->> docs
         ;; ensure all maps have the same keys
         (map #(select-keys % ks))
         ;; [{:k1 1 :k2 "val2"...} {:k1 3 :k2 "val4"...} ...]
         (map vals)
         ;; [[1 "val2"...] [3 "val4"...] ...]
         (map #(map str %))
         ;; [["1" "val2"...] ["3" "val4"...] ...]
         (map #(map count %))
         ;; [[1 4...] [1 4...] ...]
         (apply map (fn [& nums] (apply + nums)))
         ;; [10 40 ...]
         (interleave ks)
         ;; [:k1 10 :k2 40 ...]
         (apply hash-map))))

(defn byte-count-per-attr
  [path]
  (-> path
      core/load-obj
      value-lengths
      clojure.pprint/pprint))


(defn update-counter
  [counters v]
  (if (contains? counters v)
    (update-in counters [v] inc)
    (assoc counters v 1)))

(defn update-counters
  [counters vs]
  (reduce update-counter counters vs))

(defn count-repetitions-in-multivalued-attr
  [attr docs]
  (->> docs
       (map attr)
       (reduce update-counters {})))

(defn from-path-to-repetitions
  [attr path]
  (->> path
       core/load-obj
       core/get-docs
       (count-repetitions-in-multivalued-attr attr)))

(defn multivalued-attr-stats
  [attr path]
  (let [dict (from-path-to-repetitions attr path)
        value-counts (vals dict)]
    (clojure.pprint/pprint
     {"unique values"
      (-> dict keys count)
      "max number of repetitions"
      (apply max value-counts)
      "min number of repetitions"
      (apply min value-counts)
      "avg number of repetitions"
      (int (/ (reduce + 0 value-counts) (count value-counts)))
      "sum of unique value lengths"
      (->> dict
           keys
           (map str)
           (map count)
           (reduce + 0))})))

(defn take-top-docs-by-size-of-multivalued-attr
  [id-attr mv-attr num-docs path]
  (let [mv-attr-count (-> mv-attr (str "-count") (subs 1) keyword)]
    (->> path
         core/load-obj
         core/get-docs
         (map #(assoc % mv-attr-count (-> % mv-attr count)))
         (sort-by mv-attr-count)
         reverse
         (map #(select-keys % [id-attr mv-attr-count]))
         (take num-docs)
         clojure.pprint/pprint)))
