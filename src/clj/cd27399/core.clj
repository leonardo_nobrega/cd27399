(ns cd27399.core
  (:require [clojure.edn]))

(defn load-obj
  [path]
  (-> path
      slurp
      (subs 1)
      clojure.edn/read-string))

(defn get-docs
  [obj]
  (-> obj
      first
      second
      :body
      clojure.edn/read-string
      :docs))

(defn doc-at
  [idx path]
  (-> path
      load-obj
      get-docs
      (nth idx)))
