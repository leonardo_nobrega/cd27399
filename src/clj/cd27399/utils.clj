(ns cd27399.utils)

(def stack-trace-atom (atom ""))

(defmacro stack-trace-here
  [printer]
  `(do
     ;; save the trace to an external atom
     ;; to eliminate functions called by the printer
     (reset! stack-trace-atom
             (str "stack:\n"
                  (try (throw (new Throwable))
                       (catch Throwable t#
                         (->> t#
                              (.getStackTrace)
                              (map str)
                              (interpose "\n")
                              (apply str))))))
     (~printer @stack-trace-atom)))

(defn diffs [increasing-vals]
  (let [shifted-vals (cons 0 (drop-last 1 increasing-vals))]
    (map - increasing-vals shifted-vals)))

(defn percents [increasing-vals]
  (let [diffs (diffs increasing-vals)
        total (last increasing-vals)]
    (map #(float (* 100 (/ % total))) diffs)))
