(ns cd27399.xform
  (:require [cd27399.core :as core]
            [cenx.pack.core]))

(defn update-index
  [attr+value->index attr value]
  (let [value->index (get attr+value->index attr)]
    (assoc attr+value->index
           attr
           (if-not (contains? value->index value)
             (assoc value->index value (-> value->index keys count))
             value->index))))

(defn update-indices-for-doc
  [attr+value->index doc attrs]
  (reduce (fn [a+v->i attr]
            (let [value (get doc attr)]
              (if (sequential? value)
                (reduce #(update-index %1 attr %2) a+v->i value)
                (update-index a+v->i attr value))))
          attr+value->index
          attrs))

(defn value-indices-for-attrs
  [attrs docs]
  (reduce #(update-indices-for-doc %1 %2 attrs) {} docs))

(defn make-doc-with-values
  [attr+value->index]
  (reduce (fn [doc-with-values attr]
            (let [value->index (get attr+value->index attr)]
              (assoc doc-with-values attr
                     (->> (mapv vector
                                (vals value->index)
                                (keys value->index))
                          (sort-by first)
                          (mapv second)))))
          {}
          (keys attr+value->index)))

(defn update-values-of-doc
  [doc attr+value->index]
  (reduce (fn [doc attr]
            (let [value->index (get attr+value->index attr)
                  value (get doc attr)]
              (assoc doc attr
                     (if (sequential? value)
                       (mapv #(get value->index %) value)
                       (get value->index value)))))
          doc
          (keys attr+value->index)))

(defn compress
  "given that the attr value of each document is a sequence of strings
   runs over all documents and replaces each doc's attr value
   with a sequence of indices"
  [attrs docs]
  (let [attr+value->index (value-indices-for-attrs attrs docs)
        doc-with-values (make-doc-with-values attr+value->index)
        updated-docs (map #(update-values-of-doc % attr+value->index) docs)]
    (cons doc-with-values updated-docs)))

(defn decompress
  "given that the attr value of each document is a sequence of indices
   runs over all documents and replaces each doc's attr value
   with a sequence of strings"
  [docs]
  (let [[doc-with-values & actual-docs] docs
        attrs (keys doc-with-values)]
    (map (fn [doc]
           (reduce (fn [doc attr]
                     (let [value-arr (doc-with-values attr)
                           compressed-value (get doc attr)]
                       (assoc doc
                              attr
                              (if (sequential? compressed-value)
                                (mapv (fn [index] (get value-arr index))
                                      compressed-value)
                                (get value-arr compressed-value)))))
                   doc
                   attrs))
         actual-docs)))

(defn encoded-length
  [obj]
  (-> obj
      (cenx.pack.core/render-transit)
      count))
 
(defn encode-with-and-without-compression
  [attrs path]
  (let [docs (->> path core/load-obj core/get-docs)]
    {:with (->> docs
                (compress attrs)
                encoded-length)
     :without (encoded-length docs)}))

(defn compress-decompress-test
  [attrs path]
  (let [docs (->> path core/load-obj core/get-docs)
        compressed (compress attrs docs)
        decompressed (decompress compressed)]
    (= docs decompressed)))
