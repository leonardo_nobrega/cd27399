#!/bin/sh

# source:
# http://serverfault.com/questions/725030/traffic-shaping-on-osx-10-10-with-pfctl-and-dnctl

# Reset dummynet to default config
dnctl -f flush

# Compose an addendum to the default config: creates a new anchor
(cat /etc/pf.conf &&
        echo 'dummynet-anchor "test"' &&
        echo 'anchor "test"') | pfctl -q -f -

if [[ -z $1 ]]; then
    echo "*** remove throttling ***\n"
    echo "no dummynet quick on lo0 all" | pfctl -q -a test -f -
else
    echo "*** add throttling" $1 "Kbyte/s ***\n"

    # Configure the new anchor
    echo "dummynet out proto tcp from any to any port 8080 pipe 1" | \
        pfctl -q -a test -f -

    # Create the dummynet queue
    dnctl pipe 1 config bw $1Kbyte/s
fi

# Activate PF
pfctl -E
