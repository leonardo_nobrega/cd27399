
package cd27399;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.resource.ClassPathResourceManager;
import io.undertow.server.handlers.resource.ResourceHandler;
import io.undertow.websockets.core.AbstractReceiveListener;
import io.undertow.websockets.core.BufferedTextMessage;
import io.undertow.websockets.core.WebSocketCallback;
import io.undertow.websockets.core.WebSocketChannel;
import io.undertow.websockets.core.WebSockets;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import io.undertow.websockets.extensions.PerMessageDeflateHandshake;

import static io.undertow.Handlers.path;
import static io.undertow.Handlers.resource;
import static io.undertow.Handlers.websocket;

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

/**
 * Based on the websockets example by Stuart Douglas
 * from the Undertow distribution
 */
class TestMap extends HashMap<String, String> {
    private TestMap putCurrentTime(String key) {
        long now = System.currentTimeMillis();
        put(key, "" + now);
        return this;
    }
    public TestMap merge(String jsonStr) {
        JSONObject obj = new JSONObject(jsonStr);
        Iterator<String> it = obj.keys();
        while (it.hasNext()) {
            String k = it.next();
            put(k, obj.getString(k));
        }
        return this;
    }
    public TestMap serverReceivedRequest() {
        return putCurrentTime("server_recv");
    }
    public TestMap messageBuilt() {
        return putCurrentTime("msg_built");
    }
    public TestMap responseSentToUndertow() {
        return putCurrentTime("to_undertow");
    }
    public TestMap callbackComplete() {
        return putCurrentTime("callback");
    }
    public JSONObject toJSON() {
        return new JSONObject(this);
    }
    public int getSize() {
        return Integer.parseInt(get("size"));
    }
    public String getBandwidth() {
        return get("bw");
    }
}

class WSCallback implements WebSocketCallback {
    private TestMap map;

    public WSCallback(TestMap map) {
        this.map = map;
    }

    public void complete(final WebSocketChannel channel, Object context) {
        map.callbackComplete();
        WebSockets.sendText(map.toJSON().toString(), channel, null);
    }

    public void onError(final WebSocketChannel channel, Object context,
                        Throwable throwable) {
        System.out.println("WebSocketCallback.onError");
    }
}

class ReceiveListener extends AbstractReceiveListener {
    public String largeMessage(int sizeMB) {
        int MB = 1024 * 1024;
        StringBuffer buffer = new StringBuffer(sizeMB * MB);
        for (int i = 0; i < sizeMB * MB; i++) {
            buffer.append('0');
        }

        JSONObject obj = new JSONObject();
        obj.put("data", buffer.toString());
        return obj.toString();
    }

    private void setBandwidth(String bandwidth) {
        try {
            if (bandwidth == null) {
                bandwidth = "";
            }

            ProcessBuilder pb = new ProcessBuilder("sh", "set_bw.sh", bandwidth);
            pb.redirectErrorStream(true);
            Process p = pb.start();
            p.waitFor();

            // print output to stdout
            String line;
            InputStreamReader reader = new InputStreamReader(p.getInputStream());
            BufferedReader bufReader = new BufferedReader(reader);
            while ((line = bufReader.readLine()) != null) {
                System.out.println(line);
            }
            bufReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onFullTextMessage(WebSocketChannel channel,
                                     BufferedTextMessage message) {
        TestMap map = new TestMap();
        map.serverReceivedRequest();

        String jsonStr = message.getData();
        map.merge(jsonStr);
        String bandwidth = map.getBandwidth();
        setBandwidth(bandwidth);
        int size = map.getSize();
        String response = largeMessage(size);
        map.messageBuilt();

        map.responseSentToUndertow();
        WebSockets.sendText(response, channel, new WSCallback(map));
    }
}
    
class ConnectionCallback implements WebSocketConnectionCallback {
    @Override
    public void onConnect(WebSocketHttpExchange exchange,
                          WebSocketChannel channel) {
        ReceiveListener listener = new ReceiveListener();
        channel.getReceiveSetter().set(listener);
        channel.resumeReceives();
    }
}

public class WebSocketServer {
    public static void main(final String[] args) {
        ClassPathResourceManager resourceManager =
            new ClassPathResourceManager(WebSocketServer.class.getClassLoader(),
                                         WebSocketServer.class.getPackage());

        ResourceHandler resourceHandler = resource(resourceManager)
            .addWelcomeFiles("index.html");

        HttpHandler handler = path()
            .addPrefixPath("/myapp", websocket(new ConnectionCallback()))
            .addPrefixPath("/", resourceHandler);

        Undertow server = Undertow.builder()
            .addHttpListener(8080, "localhost")
            .setHandler(handler)
            .build();

        server.start();
    }
}
