(ns cd27399.xform-test
  (:require [cd27399.xform :as xform]
            [clojure.test :refer :all]))

(deftest test-update-index
  (= {:a {"v1" 0}}
     (xform/update-index {:a {"v1" 0}} :a "v2"))

  (= {:a {"v1" 0}}
     (xform/update-index {:a {"v1" 0}} :a "v1"))

  (= {:a {"v1" 0} :b {"v1" 0}}
     (xform/update-index {:a {"v1" 0}} :b "v1")))

(deftest test-update-indices-for-doc
  (= {:a {"v1" 0}
      :b {"v2" 0 "v3" 1}}
     (xform/update-indices-for-doc {}
                                   {:a "v1" :b ["v2" "v3"] :c ["v4"]}
                                   [:a :b])))

(deftest test-make-doc-with-values
  (= {:a ["v1" "v2" "v3"]
      :b ["v4"]}
     (xform/make-doc-with-values {:a {"v1" 0 "v2" 1 "v3" 2}
                                  :b {"v4" 0}}))
  (= {:a ["v3" "v2" "v1"]}
     (xform/make-doc-with-values {:a {"v1" 2 "v2" 1 "v3" 0}})))

(deftest test-update-values-of-doc
  (= {:a [2 0]
      :b 1
      :c ["v6" "v7"]}
     (xform/update-values-of-doc {:a ["v3" "v1"]
                                  :b "v5"
                                  :c ["v6" "v7"]}
                                 {:a {"v1" 0 "v2" 1 "v3" 2}
                                  :b {"v4" 0 "v5" 1}})))

(deftest test-compress
  (= (list {:a ["v1" "v2" "v5"] :b ["v3" "v6"]}
           {:a [0 1] :b 0 :c "v4"}
           {:a [2] :b 1 :d 321})
     (xform/compress [:a :b]
                     [{:a ["v1" "v2"] :b "v3" :c "v4"}
                      {:a ["v5"] :b "v6" :d 321}])))

(deftest test-decompress
  (= ({:a ["v4" "v2"] :b "v5" :c ["v8" "v9"]}
      {:a ["v1"] :d 123 :b ["v6" "v6"]})
     (xform/decompress [{:a ["v1" "v2" "v3" "v4"]
                         :b ["v5" "v6" "v7"]}
                        {:a [3 1] :b 0 :c ["v8" "v9"]}
                        {:a [0] :b [1 1] :d 123}])))
